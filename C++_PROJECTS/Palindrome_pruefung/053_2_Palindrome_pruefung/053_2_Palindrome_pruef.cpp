#include <iostream>
#include <string>
#include <algorithm>

using namespace std;


//	Zugriff auf das letzte zeichen eines strings
//	cout << word.at(word.length() -1) << endl;

bool palindrom(string word) {
	//	(start bei i = 0 ; i = 0 solange nicht groesser als index string word ; immer plus eins)
	for (int i = 0; i < word.length(); i++) {
		// ist erster und letzter buchstabe gleich -1 -i check das - zugriff auf letzen buchstaben
		if (word.at(i) == word.at(word.length() - 1 - i)) {
			cout << word << " ist ein Palindrom" << endl;
			// gebe true = 1 aus
			return true;
		}
		// wenn nicht true = 1 gebe false = 0 aus
		else {
			cout << word << " ist kein Palindrom" << endl;
			return false;
		}
	}	
}


int main(int argc, const char* argv[]) {
	
	cout << " PALINDROMTEST. Bitte ein Wort Ihrer Wahl eingeben: " << endl;
	string test;
	cin >> test;

	cout << palindrom(test) << endl;


	// strings werden hochgereicht in funktion
	cout << palindrom("ABBA") << endl;
	cout << palindrom("OTTO") << endl;
	cout << palindrom("REITTIER") << endl;
	cout << palindrom("SIMONXXXAAA") << endl;
	cout << palindrom("RELIEFPFEILER") << endl;


	return 0;
}