
#include <iostream>
#include <string>

using namespace std;
/*
	Code Erläuterungen

	variable out wird mit unterstrichen des gesuchten wortes ausgegeben
	leerer string und zugriff auf laenge des wortes ueber for schleife
	anhang von unterstrichen fuer anzahl buchstaben im gesuchten wort
	ausgabe in der konsole

	failed ist die abbruchbedingung fuer das spiel
*/

int main(int argc, const char* argv[]) {
	string word = "GALGENMAENNCHEN";
	string out = "";
	for (int i = 0; i < word.length(); i++) {
		out.append("_");
	}
	
	cout << "folgendes Word wird gesucht: " << out << endl;

	int failed = 0;

	// solange nicht alle unterstriche weg sind UND fehlversuche kleiner als 10 wird programm durchlaufen
	while (out.find("_") != string::npos && failed < 10) {
		// einlesen der versuche
		char input;
		cin >> input;

		// wenn gesuchtes wort abgleich mit eingegbenem buchstaben nicht stimmt dann fehlversuch hochzaehlen
		if (word.find(input) == string::npos) {
			failed++;
		}
		else {
			// eingelesener buchstabe check ob gleich mit gesuchtem wort
			// wenn ja dann ausgabe buchstabe statt unterstriche
			for (int i = 0; i < word.length(); i++) {
				if (word[i] == input) {
					out[i] = input;
				}
			}
		}

		// zwischenmeldung zu gefundenen buchstaben und versuchen
		// dynamisch hochzaehlen der versuche
		cout << "Folgendes Wort wird gesucht: " 
			 << out << ", du hast noch "
			 << (10 - failed)
			 << " Fehlversuche" << endl << endl;
	}

	// gibt es noch unterstriche im gesuchten wort? 
	// string::npos bedeutet keine unterstriche mehr dann ausgabe gewonnen
	if (out.find("_") == string::npos) {
		cout << "Du hast gewonnen!" << endl;
	}
	// in alle anderen faellen hat man verloren
	else {
		cout << "Du hast verloren!" << endl;
	}

	return 0;
}